# Peergos

Peergos is a p2p end-to-end encrypted storage, social media and application protocol and reference implementation on top of IPFS. The goal of Peergos is to provide a global multi-user filesystem that provides identity, privacy, login, and secure sharing for decentralized applications. It is designed to be independent of DNS and TLS certificate authorities, and to protect privacy through quantum resistant encryption.

Peergos was [started in 2013](https://peergos.org/about) with the aim of protecting people online and restoring ownership over data.

### Identity

Peergos users are identified by unique usernames linked to public keys. The uniqueness of usernames is ensured through a global append-only log for [public key to username](https://book.peergos.org/architecture/pki.html) mappings that is mirrored on every node in the Peergos system. Names are taken on a first come first served basis. Currently, a single server determines the canonical state of this log, and other nodes sync to it. Long-term considerations include decentralizing the name server through a blockchain architecture.

Peergos allows [multi-device login](https://book.peergos.org/features/multi.html) through a password-based interface. A user's private keys are derived every time they log in using their username, password and a published salt. Specifically, a signing keypair, boxing keypair, and symmetric key is derived. Users store their friend's keys in their encrypted storage space in a TOFU keystore. Users can verify their friend's keys out of band using QR codes or fingerprints.

A user's identity is independent of their current storage server. This means they can move servers transparently and maintain their full social graph, and all existing capabilities/links to files continue to work. 

### Network

Each user must be registered to at least one Peergos server (A server can host any number of users and a user can mirror their data on as many servers as they like). Peergos servers run an instance of IPFS, which handles networking and connection management. A server is any device storing user data, and could be a mobile phone, a cloud server, or hardware plugged in at home.

### Data

Data in Peergos is content-addressed: stored in mappings from hash to data. During upload the client splits files into 5 MiB chunks which are each independently encrypted (along with encrypted metadata) and stored in a [merkle-CHAMP](https://book.peergos.org/architecture/champ.html) (compressed hash array mapped prefix trie) in IPFS. Directories can't be distinguished from small files. To hide file sizes and split files up into 5 MiB chunks which aren't linkable, the size within a chunk is also rounded up (padded before encryption) to a multiple of 4 KiB. This means all chunks can only have 1 of 1280 possible sizes. Servers do not have visibility into the file sizes, the number of files, directory structure, or which users have access. All blocks of data are store in dag-cbor format of [IPLD](https://docs.ipld.io/).

The user publishes the IPFS node ID (hash of its public key) of their server in the PKI. This allows writes and new friend requests to be securely tunneled to their server. It synchronizes their writes and publishes their latest root hashes.

### Social & Discovery

Users can follow each other. [Follow requests](https://book.peergos.org/architecture/follow.html) are sent through a user’s storage server, which is contacted via its public key. Follows are one-way, and allow sharing files and sending messages. Critically, the server never sees who is following who (even follow requests are blinded). Users store their own social graph encrypted in their Peergos space. Once a follow request has been accepted, then a secure channel is opened up for sharing capabilities. Users can either look up people to add by their username, or new friends can be discovered by existing friends re-sharing a post from them. Friend keys can be verified directly using a QR code in person or by reading a number sequence using the same protocol as Signal. 

### Privacy & Access Control

All encryption happens on the client, which could be a native Peergos client or a browser. Data is always encrypted on the servers. Servers do not have access to metadata or sensitive information.

Access is controlled through cryptographic capabilities. Access is hierarchical, and stored in an encrypted structure called [cryptree](https://book.peergos.org/security/cryptree.html).

A read-only capability consists of the hash of the file owner's public key, the hash of the writer's public key, a random label, and a symmetric encryption key. Access to files gained through social follows can be revoked by rotating cryptographic keys, but the interface does not display keys to users. Users simply click "revoke access to <user>".

To make a file or folder publicly visible, a user can publish its capability. A user can also share secret links to files, like a google doc "share" link, which lets anyone who views it view the file. These [secret links](https://book.peergos.org/features/secret.html) don't expose the file to the server. The file is not transmitted unencrypted over the network, as the key to decrypt it is in the URL itself (in the hash fragment which isn't sent to the server), and is interpreted locally in the browser.

A writable capability includes the private key corresponding to the writer key, which is used to sign updates.

Peergos has undergone an independent security [audit by Cure53](https://peergos.org/posts/security-audit).

### Scalability

Peergos can handle arbitrarily large files, including random access, upload and download, and on under-powered devices like mobile phones. This is largely due to the independent encryption of each 5 MiB section, as well as the "[zero IO](https://peergos.org/posts/fast-seeking)" seeking within a file.

### Governance & Business Models

Peergos was developed by the [core team](https://peergos.org/about) on a [self-funded](https://donorbox.org/peergos) volunteer basis for years. It has received grants from [Protocol Labs](https://peergos.org/posts/dev-update), the company that stewards IPFS, from [Santander](https://twitter.com/oxfoundry/status/1232766848816549888) and most recently as [Next Generation Internet Acrhictects](https://peergos.org/posts/next-generation-internet) from NGI Pointer (part of Horizon 2020). They plan to use a business model aligned with the [interests of end users](https://peergos.org/posts/the-social-dilemma) to fund development. 

### Implementations

Peergos is a private and access controlled layer on top of IPFS which can be used to build applications. Using the Peergos reference implementation as a substrate (which implements a global social filesystem), there are a few demo [applications](https://peergos.org/posts/applications), including video streaming, a PDF viewer, text editor, [todo board/kanban](https://peergos.org/posts/private-kanban), [private calendar](https://peergos.org/posts/calendar), a social feed and [p2p web hosting](https://peergos.org/posts/p2p-web-hosting).

The goal of allowing people to deploy apps on Peergos that are viewable from the browser is currently limited by [sandboxing constraints](https://kevodwyer.github.io/sandbox/) within browsers. [COOP/COEP](https://web.dev/why-coop-coep/) increase the security of in-browser applications. 

### Links

- [Peergos](https://peergos.org/)
- [Peergos book](https://book.peergos.org)
- [Source code](https://github.com/peergos/peergos)
- [COOP and COEP Explainer](https://docs.google.com/document/d/1zDlfvfTJ_9e8Jdc8ehuV4zMEu9ySMCiTGMS9y0GU92k/edit)
